import React, { Component } from "react";

export default class HeaderComponent extends Component {
  render() {
    return (
      <h1
        className="p-5 text-center text-white"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.5)" }}
      >
        TRY GLASSES APP ONLINE
      </h1>
    );
  }
}
