import "./App.css";
import HeaderComponent from "./HeaderComponent";
import RenderGlassesList from "./RenderGlassesList";

function App() {
  return (
    <div>
      <HeaderComponent />
      <RenderGlassesList />
    </div>
  );
}

export default App;
