import React, { Component } from "react";
import { dataGlasses } from "./data";

export default class RenderGlassesList extends Component {
  state = {
    selectedGlass: null,
  };
  handleClick = (id) => {
    const glass = dataGlasses.find((glasses) => glasses.id === id);
    this.setState({ selectedGlass: glass });
    // console.log(glass.url);
  };
  render() {
    const glasses = dataGlasses.map((glasses) => (
      <div key={glasses.id}>
        <img
          src={glasses.url}
          style={{ width: "100%", cursor: "pointer" }}
          alt={glasses.name}
          onClick={() => {
            this.handleClick(glasses.id);
          }}
        />
      </div>
    ));

    return (
      <div>
        <div className="container d-flex justify-content-between align-items-center mb-5">
          <div style={{ position: "relative", width: "30%", maxWidth: "30%" }}>
            <img
              src="glassesImage/model.jpg"
              style={{ width: "100%" }}
              alt=""
            />

            {this.state.selectedGlass && (
              <div
                className="image-cover"
                style={{
                  position: "absolute",
                  width: "100%",
                  top: "25%",
                  left: "25%",
                }}
              >
                <img
                  src={this.state.selectedGlass.url}
                  style={{ width: "50%", opacity: 0.7 }}
                  alt=""
                />
              </div>
            )}
            {this.state.selectedGlass && (
              <div
                className="bottom"
                style={{
                  position: "absolute",
                  width: "100%",
                  bottom: "0",
                  left: "0",
                  backgroundColor: "rgb(255,170,102,0.5)",
                }}
              >
                <h2 style={{ color: "blue" }}>
                  {this.state.selectedGlass.name}
                </h2>
                <p className="text-white">{this.state.selectedGlass.desc}</p>
              </div>
            )}
          </div>
          <div style={{ position: "relative", width: "30%" }}>
            <img
              src="glassesImage/model.jpg"
              style={{ width: "100%" }}
              alt=""
            />
          </div>
        </div>
        <div className="container pt-5 pb-5 d-flex">{glasses}</div>
      </div>
    );
  }
}
